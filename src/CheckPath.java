import com.sun.javafx.iio.ios.IosDescriptor;

import java.io.File;
import java.io.IOException;

public class CheckPath {

    private final static String ruta_raiz = "/root/carga-curvas";/*"/home/dev3/Escritorio/curvas"*/;//
    private final static String relativa_comprimidas = "/curvasComprimidas";
    private final static String relativa_subidas = "/curvas/";
    private final static String ENDESA = "/Endesa/";
    private final static String IBERTROLA = "/Iberdrola/";
    private final static String FENOSA = "/Fenosa/";
    private final static String ELECTRICACADIZ = "/ElectricaCadiz/";
    private final static String ruta_descompresion_alternativa = "/root/carga-curvas/curvas_descomprimidas";


    public void arrancarAlternativo () throws IOException{
       new File("/root/carga-curvas/curvas_descomprimidas").mkdirs();
    }

    public void destruirDirectoriosAlternativos(){
        new File("/root/carga-curvas/curvas_descomprimidas").delete();
    }
    public void arrancar () throws IOException{
        boolean Exception = false;
        File fichero_comprimidas_ibertrola = new File(ruta_raiz+relativa_comprimidas+IBERTROLA);
        File fichero_comprimidas_fenosa = new File(ruta_raiz+relativa_comprimidas+FENOSA);
        File fichero_comprimidas_endesa = new File(ruta_raiz+relativa_comprimidas+ENDESA);
        File fichero_comprimidas_cadiz = new File(ruta_raiz+relativa_comprimidas+ELECTRICACADIZ);
        File fichero_subidas = new File(ruta_raiz+relativa_subidas);
        File fichero_ibertrola = new File(ruta_raiz+IBERTROLA);
        File fichero_endesa = new File(ruta_raiz+ENDESA);
        File fichero_fenosa = new File(ruta_raiz+FENOSA);
        File fichero_cadiz = new File(ruta_raiz+ELECTRICACADIZ);
        File fichero_alternativo = new File(ruta_descompresion_alternativa);

        if ( !fichero_comprimidas_cadiz.exists())
            if ( !fichero_comprimidas_cadiz.mkdirs())
                Exception = true;

        if ( !fichero_comprimidas_endesa.exists()){
            if ( !fichero_comprimidas_endesa.mkdirs())
                Exception = true;
        }
        if ( !fichero_comprimidas_ibertrola.exists()){
            if ( !fichero_comprimidas_ibertrola.mkdirs())
                Exception = true;
        }
        if ( !fichero_comprimidas_fenosa.exists()){
            if ( !fichero_comprimidas_fenosa.mkdirs())
                Exception = true;
        }
        if ( !fichero_subidas.exists()){
            if ( !fichero_subidas.mkdirs())
                Exception = true;
        }
        if ( !fichero_ibertrola.exists()){
            if ( !fichero_ibertrola.mkdirs())
                Exception = true;
        }
        if ( !fichero_endesa.exists()){
            if ( !fichero_endesa.mkdirs())
                Exception = true;
        }
        if ( !fichero_fenosa.exists()){
            if ( !fichero_fenosa.mkdirs())
                Exception = true;
        }
        if ( !fichero_alternativo.exists()){
            if ( !fichero_alternativo.mkdirs())
                Exception = true;
        }
        if ( !fichero_cadiz.exists())
            if ( !fichero_cadiz.mkdirs())
                Exception = true;
        if ( Exception){
            throw new IOException("No se ha podido crear alguno de los directorios necesarios, el sistema será abortado.");
        }
    }

    public String getRutaRaiz (){return this.ruta_raiz;}
    public String getRutaComprimidasEndesa(){return ruta_raiz+relativa_comprimidas+ENDESA;}
    public String getRutaComprimidasCadiz(){return ruta_raiz+relativa_comprimidas+ELECTRICACADIZ;}
    public String getRutaComprimidaIbertrola(){return ruta_raiz+relativa_comprimidas+IBERTROLA;}
    public String getRutaComprimidaFenosa(){return ruta_raiz+relativa_comprimidas+FENOSA;}
    public String getRutaCurvas(){return ruta_raiz+relativa_subidas;}
    public String getRutaEndesa(){return ruta_raiz+ENDESA;}
    public String getRutaIbertrola(){return ruta_raiz+IBERTROLA;}
    public String getRutaFenosa(){return ruta_raiz+FENOSA;}
    public String getRutaDescompresionAlternativa (){return this.ruta_descompresion_alternativa;}

    public String getRutaCadiz (){return ruta_raiz+ELECTRICACADIZ;}
    public String getRutaComprimidaCadiz(){return  ruta_raiz+relativa_comprimidas+ELECTRICACADIZ;}
}

