import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonObject;
import com.mongodb.client.model.Filters;
import jdk.nashorn.internal.codegen.CompilerConstants;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import javax.swing.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.lang.management.MemoryUsage;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.concurrent.Callable;

public class ChargueFilesV2  {

    private CheckPath checker = new CheckPath();

    private Mongo conexion;
    private static final String ruta_server = "/var/www/vhosts/ganaenergia.com/neuro/cch";
    private static final String ruta_fact_1 = "01_Salida_FACT";
    private static final String ruta_val_1 = "02_Salida_VAL";
    private static final String ruta_fact_2 = "01_SALIDA_FACT";
    private static final String ruta_val_2 = "02_SALIDA_VAL";
    private int dias;


    public ChargueFilesV2(Mongo conexion, int dias){
        this.conexion = conexion;
        this.dias = dias;
    }

    public ArrayList<String> call(){
        JsonObject arreglo = new JsonObject();
        File[] primer_nivel = new File(this.ruta_server).listFiles();
        String[] primer_listado =new String[primer_nivel.length];
        int x = 0;
        for ( File directorio : primer_nivel){
            primer_listado[x] = directorio.getAbsolutePath();
        }
        primer_nivel = null;
        ArrayList<String> retorno = new ArrayList<>();
        //Vamos a sacar ahora el listado de los directorios Salida_fact y Salida_Val, Empezamos con las VAL. La estructura será.
        File[] listado_fichero = new File[primer_listado.length];
        for ( String nivel_uno : primer_listado) {
            try {
                listado_fichero = new File(nivel_uno + "/" + ruta_val_1).listFiles();
            } catch (NullPointerException np1) {
                try {
                    listado_fichero = new File(nivel_uno + "/" + ruta_val_2).listFiles();
                } catch (NullPointerException np2) {}
            }
            long j = 0;;
            try {
                if ( listado_fichero != null) {
                    if (listado_fichero.length != 0) {
                        for (File fichero_val : listado_fichero) {
                            int dias = Days.daysBetween(new DateTime(fichero_val.lastModified()), new DateTime()).getDays();
                           // if (fichero_val.getName().toUpperCase().contains("P5D") && (dias < this.dias )) {
                                retorno.add(fichero_val.getAbsolutePath());
                          //  }

                        }
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            try {
                listado_fichero = new File(nivel_uno + "/" + ruta_fact_1).listFiles();
            } catch (Exception ex) {
                ex.printStackTrace();
                listado_fichero = new File(nivel_uno + "/" + ruta_fact_2).listFiles();
            }

            j = 0;
            try {
                if (listado_fichero != null) {
                    if (listado_fichero.length != 0) {
                        for (File fichero_fact : listado_fichero) {
                            int dias = Days.daysBetween( new DateTime(fichero_fact.lastModified()), new DateTime()).getDays();
                            //if (fichero_fact.getName().toUpperCase().contains("F5D") && (dias < this.dias)) {
                                retorno.add(fichero_fact.getAbsolutePath());
                          //  }
                        }
                    }
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }
            listado_fichero = null;
        }
        return retorno;

    }


}
