import java.util.ArrayList;
class Libreriav2 {

    private static final String ZIP = "unzip  -oq {origen} -d {destino}";
    private static final String GZIP = "gunzip -c {origen} > {destino}";
    private static final String BZ2 = "bzip2 -dc {origen} > {destino}";

    public void LibreriaV2(ArrayList<String> arreglo){
        CheckPath rutas = new CheckPath();
        long x = 1;
        for ( String fichero : arreglo){
            String cmd = "";
            if ( fichero.toLowerCase().contains(".zip") && !fichero.toLowerCase().contains("sha")){
                cmd = ZIP.replace("{origen}", fichero);
            }else if ( fichero.toLowerCase().contains(".gz2") && !fichero.toLowerCase().contains("sha")){
                cmd = GZIP.replace("{origen}", fichero);
            }else if ( fichero.toLowerCase().contains(".bz2") && !fichero.toLowerCase().contains("sha")){
                cmd = BZ2.replace("{origen}", fichero);
            }

            cmd = cmd.replace("{destino}", rutas.getRutaDescompresionAlternativa());
            try {
                int pr = new ProcessBuilder("/bin/bash", "-c", cmd).start().waitFor();
                Thread.sleep(100);
                if (pr != 0) {
                }
                x++;
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}

