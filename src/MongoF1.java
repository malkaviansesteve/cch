import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

public class MongoF1 {
    private  String uriProd = "mongodb://gana_crm:ZXLxqMMyp93pVoX6pTH369pkH@blade.ganaenergia.com:27017,clara-oswald.ganaenergia.com:27017,rose.ganaenergia.com:27017/crm?replicaSet=prod&w=majority";
    private MongoClient MongoF1 = null;
    private String database = null;
    public MongoF1(String url){
        if(this.isProd()){
            url = this.uriProd;
        }
        MongoClientURI uri = new MongoClientURI(url);
        database = uri.getDatabase();
        MongoF1 = new MongoClient (uri);
    }

    private boolean isProd(){
        try {
            File fichero = new File("/etc/hostname");
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fichero)));
            String hostmane = br.lines().collect(Collectors.joining()).toLowerCase();
            if ( hostmane.contains("clara") || hostmane.contains("clara-oswald") || hostmane.contains("rose") || hostmane.contains("blade")) {
                return true;
            }else{
                return false;
            }
        }catch (Exception ex){
            return true;
        }
    }

    public MongoCollection<Document> getCollection(String collection) {
        try {

            MongoDatabase db = MongoF1.getDatabase ( database );
            return db.getCollection(collection);

        } catch (Exception ex) {
            return null;
        }
    }
    public MongoCollection<Document> getF1v1 (){ return this.getCollection("LecturasF1");}
}
