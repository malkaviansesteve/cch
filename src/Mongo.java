import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

public class Mongo {
    private  String uriProd = "mongodb://sips:qfweKs1CZkMttYewaQFY@blade.ganaenergia.com:27017/sips?replicaSet=prod";
    private MongoClient mongo = null;
    private String database = null;
    public Mongo(String url) {
        if(this.isProd()){
            url = this.uriProd;
        }
        MongoClientURI uri = new MongoClientURI(url);
        database = uri.getDatabase();
        mongo = new MongoClient (uri);
    }

    private boolean isProd(){
        try {
            File fichero = new File("/etc/hostname");
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fichero)));
            String hostmane = br.lines().collect(Collectors.joining()).toLowerCase();
            if ( hostmane.contains("clara") || hostmane.contains("clara-oswlad") || hostmane.contains("rose") || hostmane.contains("blade")) {
                return true;
            }else{
                return false;
            }
        }catch (Exception ex){
            return true;
        }
    }

    public MongoCollection<Document> getCollection(String collection) {
        try {

            MongoDatabase db = mongo.getDatabase ( "curvas" );
            return db.getCollection(collection);

        } catch (Exception ex) {
            return null;
        }

    }

    public MongoCollection<Document> getCCHValidadas (){
        System.out.println("Accediendo a curvasFacturablesP5V2");
        return this.getCollection("curvasFacturablesP5V2");
    }
    public MongoCollection<Document> getCCHFacturables (){
        System.out.println("Accediendo a curvasFacturablesF5V2");
        return this.getCollection("curvasFacturablesF5V2");
    }



}
