import com.eclipsesource.json.JsonObject;

public class ModelHora {

    private double energiaEntrante = 0;
    private int bandera = 0;

    public double getEnergiaEntrante() {
        return energiaEntrante;
    }

    public void setEnergiaEntrante(double energiaEntrante) {
        this.energiaEntrante = energiaEntrante;
    }

    public int getBandera() {
        return bandera;
    }

    public void setBandera(int bandera) {
        this.bandera = bandera;
    }



    public JsonObject ToJsonObject(){
        return new JsonObject()
               .add("energiaEntrante", this.getEnergiaEntrante())
               .add("bandera", this.getBandera());


    };

    public void NullearHora(){
        this.setBandera(0);
        this.setEnergiaEntrante(0);
    }
}
