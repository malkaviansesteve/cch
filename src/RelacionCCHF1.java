import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoIterable;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import org.bson.types.ObjectId;

public class RelacionCCHF1 extends Thread{

    MongoCollection<Document> F5D;
    MongoCollection<Document> F1;
    public RelacionCCHF1(Mongo conexion, MongoF1 conexionf1){
        this.F1 = conexionf1.getF1v1();
        this.F5D = conexion.getCCHFacturables();
    }

    public void run (){
        if ( F5D.countDocuments(Filters.exists("factura", false)) != 0){
            MongoIterable<Document> cursor = F5D.find(Filters.exists("factura", false)).projection(new Document().append("_id", 1).append("id_factura", 1));

            for ( Document document: cursor ){
                try{
                    while (true ) {
                        int numero = ((int)(Math.random() * 100)+1) ;
                        if ( numero > 50) {
                            Thread.sleep(numero);
                            break;
                        }
                    }
                }catch (Exception ex){

                }
               String id_factura_cch = Json.parse(document.toJson()).asObject().get("id_factura").asString();
               String oid_f1 = Json.parse(F1.find(
                       Filters.eq("id_factura", id_factura_cch))
                       .projection(new Document().append("_id", 1)).first().toJson()).asObject().get("_id").asObject().get("$oid").asString();

               F5D.updateOne(Filters.eq("_id",new ObjectId(Json.parse(document.toJson()).asObject().get("_id").asObject().get("$oid").asString()) ),
                       new Document()
                               .append("$set",
                                       new Document().append("factura",
                                               new Document().append("oid", new ObjectId(oid_f1)).append("id_factura", id_factura_cch)))
                               .append("$unset", new Document().append("id_factura", ""))
               );

            }
        }
    }
}
