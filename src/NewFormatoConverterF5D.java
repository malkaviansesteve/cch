import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonObject;
import com.mongodb.client.MongoCollection;
import org.bson.Document;

public class NewFormatoConverterF5D extends Thread {

    private MongoCollection<Document> F5D;
    private MongoCollection<Document> F5Dv2;

    public NewFormatoConverterF5D (MongoCollection<Document> F5D, MongoCollection<Document> F5Dv2){
        this.F5D = F5D;
        this.F5Dv2 = F5Dv2;
    }


    public void run() {
        JsonObject new_curva = new JsonObject();

        long registros = 0;
        for ( Document curva : this.F5D.find()){
            registros++;
            try{
                Thread.sleep(200);
            }catch (Exception ex){

            }
            JsonObject curva_old = Json.parse(curva.toJson()).asObject();
            new_curva.add("cups", curva_old.get("cups").asString().substring(0, 20))
                    .add("dia", curva_old.get("dia").asInt())
                    .add("mes",  curva_old.get("mes").asInt())
                    .add("anio",  curva_old.get("anio").asInt())
                    .add("timestamp", curva_old.get("timestamp").asLong())
                    .add("archivos", curva_old.get("archivos").asArray());

            JsonObject horas = new JsonObject();
            int cambio_bandera = 0;
            //cambio_bandera == 0 -> Primera vez, 1 Guardada la bandera, 2 -> Cambio bandera detectado.
            int bandera = 0;
            for ( int x = 0; x <= 24; x++){
                JsonObject hora = new JsonObject();
                boolean no_esta = false;
                try{
                    hora = curva_old.get("hora"+x).asObject();
                }catch (NullPointerException ex){
                    no_esta = true;
                }
                if ( !no_esta){
                    hora.add("hora", x);
                    if ( cambio_bandera == 0){
                        bandera = hora.get("bandera" ).asInt();
                        cambio_bandera = 1;
                    }else if ( cambio_bandera == 1){
                        if ( bandera != hora.get("bandera").asInt()){
                            bandera =  hora.get("bandera").asInt();
                            cambio_bandera = 2;
                        }
                    }
                    horas.add(String.valueOf(x), hora.get("energiaEntrante").asInt());
                }else{
                    try {
                        if (new_curva.get("cambio_bandera").asBoolean() && x == 24) {
                            horas.add(String.valueOf(x), Json.NULL);
                        }
                    }catch (Exception ex){}
                }

            }
            if ( cambio_bandera == 1){
                new_curva.add("bandera", bandera).add("cambio_bandera", false);
            }else{
                new_curva.add("bandera", bandera).add("cambio_bandera", true);
            }
            new_curva.add("horas", horas);

            try {
                F5Dv2.insertOne(Document.parse(new_curva.toString()));
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }
}
