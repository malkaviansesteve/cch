import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import org.bson.Document;

import javax.management.RuntimeErrorException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;

public class OneFile extends Thread{
    private String linea;
    private boolean curva_fact;
    private String nombre_fichero;
    private String ruta_completa;
    private long validadas = 0;
    private long facturables = 0;
    private MongoCollection<Document> curvas_validadas;
    private MongoCollection<Document> curvas_facturables;


    public OneFile(String linea, Mongo conexion, boolean curva_fact, String nombre_Fichero, String ruta_completa) throws  RuntimeErrorException{
        this.linea = linea;
        this.curvas_facturables = conexion.getCCHFacturables();
        this.curvas_validadas = conexion.getCCHValidadas();
        this.curva_fact = curva_fact;
        this.nombre_fichero = nombre_Fichero;
        this.ruta_completa = ruta_completa;
    }
    //todo Estructura
    //      "hora10" : {
    //        "energiaEntrante" : 27,
    //        "bandera" : 0
    //    },

    @Deprecated
    public void run() throws RuntimeErrorException {
        /* Estructura de los ficheros P5D
         *  0 - CUPS, 1- Fecha/Hora, 2- Bandera,
         *  3 - Activa Entrante, 4 - Activa Saliente
         */
        /*Estructura de las F5D
         * 0- CUPS, 1-Fecha, 2 - Bandera 3- Activa Entrante
         * 4- Activa saliente, 5- Reactiva 1, 6-Reactiva 2, 7- Reactiva 3, 8- Reactiva 4,
         * 9.- Si Metodo de obtencion, (no lo tenemos en cuenta), 10 -Indicador de Firmeza
         * 11 - Codigo de Factura
         */

        long numero = 0;
        while (numero < 100)
            numero = (long) (Math.random() * 500) + 1;
        try {
            Thread.sleep(numero);

        }catch (Exception ex){}
        String cups = "";
        JsonObject cch = new JsonObject();
        int hora_int = 0;
        int dia = 0;
        int anio = 0;
        int mes = 0;
        String[] split = linea.split(";");
        try {
            String fecha_parseada = split[1].replaceAll("/", "-");
            String[] fecha_hora = fecha_parseada.split(" ");//0 fecha 1 hora
            String[] split_hora = fecha_hora[1].split(":");//0 hora 1 minutos
            String[] fecha_split = fecha_hora[0].split("-");
            hora_int = Integer.parseInt(split_hora[0]);
            dia = Integer.parseInt(fecha_split[2]);
            anio = Integer.parseInt(fecha_split[0]);
            mes = Integer.parseInt(fecha_split[1]);

            cups = split[0];
            if (curva_fact)
                cch.add("codigo_factura", split[10]);
            ModelHora hora = new ModelHora();
            hora.setBandera(Integer.parseInt(split[2]));
            try {
                hora.setEnergiaEntrante(Double.parseDouble(split[3]));
            }catch (Exception ex){
                hora.setEnergiaEntrante(0);
            }
            cch.add("cups", cups).add("dia", dia).add("mes", mes).add("anio", anio).add("archivos", new JsonArray().add(nombre_fichero)).add("hora" + hora_int, hora.ToJsonObject());
            long countDocuments = 0;

            if (curva_fact) {
                countDocuments = curvas_facturables.countDocuments(
                        Filters.and(
                                Filters.eq("cups", cups),
                                Filters.eq("dia", dia),
                                Filters.eq("mes", mes),
                                Filters.eq("anio", anio)
                        )
                );
            } else {
                countDocuments = curvas_validadas.countDocuments(
                        Filters.and(
                                Filters.eq("cups", cups),
                                Filters.eq("dia", dia),
                                Filters.eq("mes", mes),
                                Filters.eq("anio", anio)
                        )
                );
            }

            if (countDocuments == 0) {

                if (curva_fact) {
                    facturables++;
                    curvas_facturables.insertOne(
                            Document.parse(cch.toString())
                    );
                } else {
                    validadas++;
                    curvas_validadas.insertOne(Document.parse(cch.toString()));
                }
            } else {
                if (curva_fact) {
                    facturables++;
                    curvas_facturables.updateOne(
                            Filters.and(
                                    Filters.eq("cups", cups),
                                    Filters.eq("dia", dia),
                                    Filters.eq("mes", mes),
                                    Filters.eq("anio", anio)
                            ),
                            Document.parse(new JsonObject().add("$set", new JsonObject().add("hora" + hora_int, hora.ToJsonObject())).toString()));


                } else {
                    validadas++;
                    curvas_validadas.updateOne(
                            Filters.and(
                                    Filters.eq("cups", cups),
                                    Filters.eq("dia", dia),
                                    Filters.eq("mes", mes),
                                    Filters.eq("anio", anio)
                            ),
                            Document.parse(new JsonObject().add("$set", new JsonObject().add("hora" + hora_int, hora.ToJsonObject())).toString()));

                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
            System.exit(1);
        }

    }

}
