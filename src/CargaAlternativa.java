import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import org.joda.time.DateTime;
import org.joda.time.Days;

import javax.swing.text.Document;
import java.io.File;
import java.util.concurrent.Callable;

public class CargaAlternativa{

    private static final String ruta_server = "/var/www/vhosts/ganaenergia.com/neuro/cch";
    private static final String ruta_fact_1 = "01_Salida_FACT";
    private static final String ruta_val_1 = "02_Salida_VAL";
    private static final String ruta_fact_2 = "01_SALIDA_FACT";
    private static final String ruta_val_2 = "02_SALIDA_VAL";
    private  Mongo conexion;

    public CargaAlternativa(/*Mongo conexion*/){
        this.conexion = conexion;
    }

    public JsonObject inicio(){
        //todo el Days.getDays no está bien, añade todos al array, también mirar las extensiones y el tipo de curva.
        JsonObject arreglo = new JsonObject();
        long x = 0;
        for ( File fichero1 : new File(ruta_server).listFiles()){
            File[]lista = null;

            try{
                File[] listado = new File(fichero1.getAbsolutePath()+"/"+ruta_val_1).listFiles();
                for ( File fichero2 : listado){
                    long timestamp = fichero2.lastModified();
                    int dias = Days.daysBetween(new DateTime(), new DateTime(timestamp)).getDays();
                    if (  (dias < 100 && dias > -100) && (fichero2.getName().toLowerCase().endsWith(".zip") || fichero2.getName().toLowerCase().endsWith(".gz")
                            || fichero2.getName().toLowerCase().endsWith(".bz2")) && (fichero2.getName().toLowerCase().startsWith("f5d") || fichero2.getName().toLowerCase().startsWith("p5d")) ){
                        try{
                            x++;
                            arreglo.get("val").asArray().add(fichero2.getAbsolutePath());
                        }catch (Exception ex){
                            arreglo.add("val", new JsonArray().add(fichero2.getAbsolutePath()));
                        }
                    }

                }

            }catch (Exception ex){
                File[] listado = new File(fichero1.getAbsolutePath()+"/"+ruta_val_2).listFiles();
                for ( File fichero2 : listado){
                    long timestamp = fichero2.lastModified();
                    int dias = Days.daysBetween(new DateTime(), new DateTime(timestamp)).getDays();
                    if (  (dias < 100 && dias > -100) && (fichero2.getName().toLowerCase().endsWith(".zip") || fichero2.getName().toLowerCase().endsWith(".gz")
                            || fichero2.getName().toLowerCase().endsWith(".bz2")) && (fichero2.getName().toLowerCase().startsWith("f5d") || fichero2.getName().toLowerCase().startsWith("p5d")) ){
                        try{
                            x++;
                            arreglo.get("val").asArray().add(fichero2.getAbsolutePath());
                        }catch (Exception ex2){
                            arreglo.add("val", new JsonArray().add(fichero2.getAbsolutePath()));
                        }
                    }
                }
            }
            try{
                File[] listado = new File(fichero1.getAbsolutePath()+"/"+ruta_fact_1).listFiles();
                for ( File fichero2 : listado){
                    long timestamp = fichero2.lastModified();
                    int dias = Days.daysBetween(new DateTime(), new DateTime(timestamp)).getDays();
                    if (  (dias < 100 && dias > -100) && (fichero2.getName().toLowerCase().endsWith(".zip") || fichero2.getName().toLowerCase().endsWith(".gz")
                            || fichero2.getName().toLowerCase().endsWith(".bz2")) && (fichero2.getName().toLowerCase().startsWith("f5d") || fichero2.getName().toLowerCase().startsWith("p5d")) ){
                        try{
                            x++;
                            arreglo.get("fact").asArray().add(fichero2.getAbsolutePath());
                        }catch (Exception ex){
                            arreglo.add("fact", new JsonArray().add(fichero2.getAbsolutePath()));
                        }
                    }
                }

            }catch (Exception ex){
                File[] listado = new File(fichero1.getAbsolutePath()+"/"+ruta_fact_2).listFiles();
                for ( File fichero2 : listado){
                    long timestamp = fichero2.lastModified();
                    int dias = Days.daysBetween(new DateTime(), new DateTime(timestamp)).getDays();
                    if (  (dias < 100 && dias > -100) && (fichero2.getName().toLowerCase().endsWith(".zip") || fichero2.getName().toLowerCase().endsWith(".gz")
                    || fichero2.getName().toLowerCase().endsWith(".bz2")) && (fichero2.getName().toLowerCase().startsWith("f5d") || fichero2.getName().toLowerCase().startsWith("p5d"))){
                        try{
                            x++;
                            arreglo.get("fact").asArray().add(fichero2.getAbsolutePath());
                        }catch (Exception ex2){
                            arreglo.add("fact", new JsonArray().add(fichero2.getAbsolutePath()));
                        }
                    }
                }
            }


        }
        return arreglo;
    }
}
