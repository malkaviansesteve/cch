import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;
import com.jcraft.jsch.*;
import com.mongodb.client.model.Filters;
import com.sun.istack.internal.Nullable;
import org.bson.Document;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.List;


public class Endesa extends Thread {


    private static String USER = "1022";//"root";
    private static String PASS = "Anw3V7cd";//"PmTT9oVQ3W4MLOU";
    private static String HOST = "ftp.endesa.es";//"192.168.7.9";
    private static String path_bajada = "";
    private static String RUTA_SALIDA_FACT = "01_SALIDA_FACT/";
    private static String RUTA_SALIDA_VAL = "02_SALIDA_VAL/";
    private static Mongo conexion;
    private String ruta_raiz = "";
    private static final  CheckPath check = new CheckPath();

    public Endesa (Mongo conexion){
        this.conexion = conexion;
        this.ruta_raiz = check.getRutaRaiz();
        this.path_bajada = check.getRutaComprimidasEndesa();
    }


    @SuppressWarnings("all")
    public void run(){

        long incio = new DateTime().getMillis();
        ChannelSftp channelSftp = null;
        Channel channel = null;
        Session session = null;
        JsonArray listado_fact = new JsonArray();
        JsonArray listado_val = new JsonArray();
        long bajados = 0;
        try {
            JSch jsch = new JSch();
            session = jsch.getSession(USER, HOST, 22);
            // Establecemos las opciones de configuracion y el password, luego abrimos la conexión y establecemos el TTL a 2 minutos
            session.setConfig("StrictHostKeyChecking", "no");
            session.setConfig("PreferredAuthentications", "password");
            session.setPassword(PASS);
            session.connect();
            // Abrimos el canal para operar
            channel = session.openChannel("sftp");
            channelSftp = (ChannelSftp) channel;
            channel.connect();
            //listamos el contenido de los dos directorios
            List devolverFacturables = channelSftp.ls(RUTA_SALIDA_FACT);
            List devolverValidadas = channelSftp.ls(RUTA_SALIDA_VAL);
            String[]split;
            // Posicion del nombre del fichero = 29
            for (Object a :devolverFacturables) {
                split = a.toString().split(" ");
                for ( String string :split){
                    if ( (string.endsWith("gz") || string.endsWith("GZ")) || (string.endsWith("zip") || string.endsWith("ZIP")) || (string.endsWith("bz2") || string.endsWith("BZ2"))){
                        listado_fact.add(string);
                    }
                }
            }
            // Posicion del nombre del fichero = 29
            for (Object a :devolverValidadas) {
                split = a.toString().split(" ");
                for ( String string :split){
                    if ( (string.endsWith("gz") || string.endsWith("GZ")) || (string.endsWith("zip") || string.endsWith("ZIP")) || (string.endsWith("bz2") || string.endsWith("BZ2"))){
                        listado_val.add(string);
                    }
                }
            }


            JsonObject listado_definitivo = this.listado_ficheros(listado_fact, listado_val);

            for ( JsonObject.Member lista : listado_definitivo){
                if ( lista.getName().equals("fact")){
                    for ( JsonValue fichero : lista.getValue().asArray()){
                        try{
                        channelSftp.get(RUTA_SALIDA_FACT+"/"+fichero.asString(),  this.path_bajada);
                        }catch (SftpException sftp){
                            sftp.printStackTrace();
                            session.disconnect();
                            channel.disconnect();
                            channelSftp.disconnect();
                            return;
                        }
                    }
                }else if ( lista.getName().equals("val") ){
                    for ( JsonValue fichero : lista.getValue().asArray()){
                        try{
                        channelSftp.get(RUTA_SALIDA_VAL+"/"+fichero.asString(),  this.path_bajada);
                        }catch (SftpException sftp){
                            sftp.printStackTrace();
                            session.disconnect();
                            channel.disconnect();
                            channelSftp.disconnect();
                            return;
                        }
                    }
                }else{
                    for ( JsonObject.Member otras : lista.getValue().asObject()){
                        if ( otras.getName().equals("fact")){
                            for ( JsonValue string : otras.getValue().asArray()){
                                try {
                                    channelSftp.get(RUTA_SALIDA_FACT + "/" + string.asString(), check.getRutaCurvas());
                                }catch (SftpException sftp){
                                    sftp.printStackTrace();
                                    session.disconnect();
                                    channel.disconnect();
                                    channelSftp.disconnect();
                                    return;
                                }
                            }
                        }else{
                            for ( JsonValue string : otras.getValue().asArray()){
                                try{
                                channelSftp.get(RUTA_SALIDA_VAL+"/"+string.asString(), check.getRutaCurvas());
                                }catch (SftpException sftp){
                                    sftp.printStackTrace();
                                    session.disconnect();
                                    channel.disconnect();
                                    channelSftp.disconnect();
                                    return;
                                }
                            }
                        }
                    }
                }
            }
            //todo cuando se acabe de procesar, elimiar curva.
            //  Libreria libreria = new Libreria();
            //  libreria.DescomprimirCurvas(path_bajada, path_descomprimir);

        }catch (Exception ex){
            ex.printStackTrace();
            session.disconnect();
            channel.disconnect();
            channelSftp.disconnect();
        }
        session.disconnect();
        channel.disconnect();
        channelSftp.disconnect();
        //  Libreria libreria = new Libreria();
        // libreria.DescomprimirCurvas(path_bajada, path_descomprimir);
        //this.MoverFicherosInvalidos();

    }

    private JsonObject listado_ficheros (JsonArray entrada_fact, JsonArray entrada_val){
        int x = 0;
        DateTime dt_fact = this.getFecha(true, "");
        DateTime dt_val = this.getFecha(false, "");
        JsonObject devolver = new JsonObject();
        for (JsonValue fichero : entrada_fact) {
            String reemplazado = fichero.asString().replace(".gz", "").replace(".zip", "" )
                    .replace(".bz2", "");
            if (conexion.getCCHFacturables().countDocuments(Filters.eq("archivos",reemplazado)) == 0) {
                if (fichero.asString().contains("F5D") || fichero.asString().contains("f5d")) {
                    DateTime dt_file = this.getFecha(true, fichero.asString());
                    if ( dt_fact.getYear() <= dt_file.getYear()) {
                        //El año del fichero es mayor
                        if (dt_fact.getMonthOfYear() <= dt_file.getMonthOfYear()) {
                            //El mes del fichero es mayor
                            if (dt_fact.getDayOfMonth() <= dt_file.getDayOfMonth()) {
                                try {
                                    devolver.get("fact").asArray().add(fichero.asString());
                                    x++;
                                } catch (Exception ex) {
                                    devolver.add("fact", new JsonArray().add(fichero.asString()));
                                    x++;
                                }
                            }
                        }
                    }

                }else{
                    try {
                        devolver.get("otras").asObject().get("fact").asArray().add(fichero.asString());
                        x++;
                    }catch (NullPointerException np){
                        devolver.add("otras", new JsonObject().add("fact", new JsonArray().add(fichero.asString())));
                        x++;
                    }
                }

            }
        }
        for (JsonValue fichero : entrada_val){
            String reemplazado = fichero.asString().replace(".gz", "").replace(".zip", "" )
                    .replace(".bz2", "");
            if ( conexion.getCCHValidadas().countDocuments(Filters.eq("archivos", reemplazado)) == 0) {

                if ( fichero.asString().contains("P5D") || fichero.asString().contains("p5d")) {
                    DateTime dt_file = this.getFecha(false, fichero.asString());
                    if ( dt_val.getYear() <= dt_file.getYear()) {
                        //El año del fichero es mayor o igual
                        if (dt_val.getMonthOfYear() <= dt_file.getMonthOfYear()) {
                            //El mes del fichero es mayor o igual
                            if (dt_val.getDayOfMonth() <= dt_file.getDayOfMonth() ) {
                                try {
                                    devolver.get("val").asArray().add(fichero.asString());
                                    x++;
                                } catch (Exception ex) {
                                    devolver.add("val", new JsonArray().add(fichero.asString()));
                                    x++;
                                }
                            }
                        }
                    }

                }else{
                    try {
                        devolver.get("otras").asObject().get("val").asArray().add(fichero.asString());
                        x++;
                    }catch (NullPointerException np){
                        DateTime dt_file = this.getFecha(false, fichero.asString());
                        if ( dt_val.getYear() <= dt_file.getYear()) {
                            //El año del fichero es mayor o igual
                            if (dt_val.getMonthOfYear() <= dt_file.getMonthOfYear()) {
                                //El mes del fichero es mayor o igual
                                if (dt_val.getDayOfMonth() <= dt_file.getDayOfMonth() ) {
                                    try {
                                        devolver.get("otras").asObject().get("val").asArray().add(fichero.asString());
                                        x++;
                                    } catch (Exception ex) {
                                        devolver.add("otras", new JsonObject().add("val", new JsonArray().add(fichero.asString())));
                                        x++;
                                    }
                                }
                            }
                        }

                    }
                }

                // else
                //new File("/home/dev3/Escritorio/curvas/curvasComprimidas/Fenosa"+fichero.asString()).renameTo(new File(ruta_ficheros_no_F5_ni_P5+"/"+fichero.asString()));
            }else{
                // new File(ruta_raiz+"/curvasComprimidas/Endesa/"+fichero).delete();
            }
        }
        return devolver;
    }
    public DateTime getFecha (boolean facturables, @Nullable String file_name){
        String dia = "";
        String mes = "";
        String anio = "";
        if ( file_name.isEmpty()) {
            Document sort = new Document().append("anio", -1).append("mes", -1).append("dia", -1);
            Document proyection = new Document().append("dia", 1).append("mes", 1).append("anio", 1);
            JsonObject curva = null;
            if (facturables) {
                curva = Json.parse(conexion.getCCHFacturables().find().projection(proyection).sort(sort).first().toJson()).asObject();
                if ( curva.get("dia").asInt() < 10)
                    dia = "0"+String.valueOf(curva.get("dia").asInt());
                else
                    dia = String.valueOf(curva.get("dia").asInt());
                if ( curva.get("mes").asInt() < 10)
                    mes = "0"+String.valueOf(curva.get("mes").asInt());
                else
                    mes = String.valueOf(curva.get("mes").asInt());
                anio = String.valueOf(curva.get("anio").asInt());
            }else{

                curva = Json.parse(conexion.getCCHValidadas().find().projection(proyection).sort(sort).first().toJson()).asObject();
                if ( curva.get("dia").asInt() < 10)
                    dia = "0"+String.valueOf(curva.get("dia").asInt());
                else
                    dia = String.valueOf(curva.get("dia").asInt());
                if ( curva.get("mes").asInt() < 10)
                    mes = "0"+String.valueOf(curva.get("mes").asInt());
                else
                    mes = String.valueOf(curva.get("mes").asInt());
                anio = String.valueOf(curva.get("anio").asInt());
            }
            DateTimeFormatter dtf = DateTimeFormat.forPattern("dd/MM/YYYY");
            return dtf.parseDateTime(dia+"/"+mes+"/"+anio);
        }else{
            DateTimeFormatter dtf = DateTimeFormat.forPattern("YYYY/MM/dd");
            String[] split = file_name.split("_");
            String b = split[3].substring(0, split[3].indexOf("."));
            return dtf.parseDateTime(b.substring(0, 4) + "/" + b.substring(4, 6)+"/"+ b.substring(6, b.length()) );
        }
    }
}
