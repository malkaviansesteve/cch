import com.sun.istack.internal.Nullable;
import org.joda.time.DateTime;
import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class ChargueFiles extends Thread{

    private Mongo conexion;
    private boolean endesa;
    private boolean fenosa;
    private boolean ibertrola;
    private boolean cadiz;
    private boolean alternativo;
    private boolean is_conexion2;

    public ChargueFiles(Mongo conexion, boolean endesa, boolean fenosa, boolean ibertrola, boolean cadiz, boolean carga_alternativa){
        this.conexion = conexion;
        this.endesa = endesa;
        this.fenosa = fenosa;
        this.ibertrola = ibertrola;
        this.cadiz = cadiz;
        is_conexion2 = false;
        this.alternativo = carga_alternativa;
    }

    public void run (){
        /*try {
            FileOutputStream fout2 = new FileOutputStream("/var/www/vhosts/ganaenergia.com/XML/ficheros.txt");
            PrintStream pout2 = new PrintStream(fout2);
            System.setOut(pout2);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.exit(0);
        }*/
        CheckPath check = new CheckPath();
        ModelHora hora = new ModelHora();
        BufferedReader bf = null;
        FileReader fr = null;
        if ( !alternativo) {
            long inicio = new DateTime().now().getMillis() / 1000;
            //todo hay discursión sobre si usar el BufferedReader o el FileInputStream
            //todo el BufferedReader usa un buffer de disco, lo que carga menos la memoria
            //todo el FileInputStream es más rápido, ya que vuelca el fichero en memoria.

            String ruta_ibertrola = check.getRutaIbertrola();
            String ruta_endesa = check.getRutaEndesa();
            String ruta_fenosa = check.getRutaFenosa();
            String ruta_cadiz = check.getRutaCadiz();
            OneFile oneFile = null;
            String linea = "";
            String fichero = "";
            File[] directorio = null;
            if (endesa) {
                directorio = new File(ruta_endesa).listFiles();
            } else if (fenosa) {
                directorio = new File(ruta_fenosa).listFiles();
            } else if (ibertrola) {
                directorio = new File(ruta_ibertrola).listFiles();
            } else if (cadiz) {
                directorio = new File(ruta_cadiz).listFiles();
            } else
                throw new IllegalArgumentException("No se ha seleccionado distribuidora");


            for (File fichero_file : directorio) {
                boolean facturable = false;
                if (fichero_file.getName().toLowerCase().contains("f5d")) {
                    facturable = true;
                }
                try {
                    fichero = "";
                    fr = new FileReader(fichero_file.getAbsolutePath());
                    bf = new BufferedReader(fr);
                    long ahora = new DateTime().getMillis() / 1000;
                    while (null != (linea = bf.readLine())) {
                        oneFile = new OneFile(linea, conexion, facturable, fichero_file.getName(), fichero_file.getAbsolutePath());
                        try {
                            oneFile.start();
                            oneFile.join();
                            fichero_file.delete();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                //todo -> Borrado del fichero DESCOMPRIMIDO
                //fichero_file.delete();

            }
            directorio = null;
        }else{
            try{
                String ruta = check.getRutaDescompresionAlternativa();

                File[] directorio = new File(ruta).listFiles();
                long x = 0;
                for ( File fichero_file : directorio) {
                    boolean facturable = false;
                    long facturables = 0;
                    long validadas = 0;
                    if (fichero_file.getName().toLowerCase().contains("f5d")) {
                        facturable = true;
                    }else{
                        facturable = false;
                    }
                    try {
                        String fichero = "";
                        String linea = "";
                        if ( fichero.length() < 5242880/*5Mb*/) {
                            fr = new FileReader(fichero_file.getAbsolutePath());
                            bf = new BufferedReader(fr);
                            long ahora = new DateTime().getMillis() / 1000;
                            while (null != (linea = bf.readLine())) {
                                x++;
                                Thread.sleep(50);
                                OneFile oneFile = null;
                                oneFile = new OneFile(linea, conexion, facturable, fichero_file.getName(), fichero_file.getAbsolutePath());
                                try {
                                    oneFile.start();
                                    oneFile.join();
                                    fichero_file.delete();
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            }
                        }else{
                            FileSystem fs = FileSystems.getDefault();
                            Path ruta_grande = fs.getPath(check.getRutaDescompresionAlternativa());
                            List<String> texto= Files.readAllLines(ruta_grande);
                            for ( String cadena : texto){
                                OneFile oneFile = new OneFile(linea, conexion, facturable, fichero_file.getName(), fichero_file.getAbsolutePath());
                                try {
                                    oneFile.start();
                                    oneFile.join();
                                    fichero_file.delete();
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            }
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
                directorio = null;
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }
    public Thread GetCurrentThread (){
        return Thread.currentThread();
    }
}
