import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import org.joda.time.DateTime;

import java.io.*;


/* Estructura de los ficheros P5D
 *  0 - CUPS, 1- Fecha/Hora, 2- Bandera,
 *  3 - Activa Entrante, 4 - Activa Saliente
 */
/*Estructura de las F5D
 * 0- CUPS, 1-Fecha, 2 - Bandera 3- Activa Entrante
 * 4- Activa saliente, 5- Reactiva 1, 6-Reactiva 2, 7- Reactiva 3, 8- Reactiva 4,
 * 9.- Si Metodo de obtencion, (no lo tenemos en cuenta), 10 -Indicador de Firmeza
 * 11 - Codigo de Factura
 */


class Parseadorv2 {

    private Mongo conexion;
    private MongoCollection<Document> f1;

    public Parseadorv2(Mongo conexion, MongoF1 conexion_f1) {
        this.conexion = conexion;
        this.f1 = conexion_f1.getF1v1();
    }

    public void start() {
        CheckPath check = new CheckPath();
        BufferedReader bf = null;
        FileReader fr = null;
        String fichero_str = "";
        File[] listado = new File(check.getRutaDescompresionAlternativa()).listFiles();
        long validadas = 0;
        long facturables = 0;
        for (File fichero : listado) {
            MongoCollection<Document> collection;
            if (fichero.getName().toLowerCase().contains("f5d")) {
                collection = conexion.getCCHFacturables();
                continue;

            }else{
                collection = conexion.getCCHValidadas();
            }
            fichero_str = "";
            long joder = 0;
            long ahora = new DateTime().getMillis() / 1000;
            if (fichero.length() < 10485760/*10Mb*/) {
                try {
                    fr = new FileReader(fichero.getAbsolutePath());
                    bf = new BufferedReader(fr);

                    String linea = "";
                    while (null != (linea = bf.readLine())) {
                        joder++;
                        fichero_str += linea + "\n";
                    }
                    fr.close();
                    bf.close();
                } catch (Exception ex) {
                }
            } else {
                try {
                    String text = "";
                    int read, N = 1024 * 1024;
                    char[] buffer = new char[N];
                    fr = new FileReader(fichero);
                    BufferedReader br = new BufferedReader(fr);
                    while (true) {
                        read = br.read(buffer, 0, N);
                        fichero_str += new String(buffer, 0, read);
                        if (read < N) {
                            break;
                        }
                    }
                    fr.close();
                    bf.close();
                } catch (Exception ex) {
                }
            }
            long ahora_tambien = new DateTime().getMillis() / 1000;
            boolean primera_vez = false;
            String[] split = fichero_str.split("\n");
            String antiguo_cups = "";
            JsonObject cch_actual = new JsonObject();
            long j = 0;
            String cups_anterior = "";
            int dia_ant = 0;
            boolean existe_bbdd = false;
            for (String linea : split) {
                try {
                    while (true) {
                        int numero = ((int) (Math.random() * 100) + 1);
                        if (numero > 50) {
                            Thread.sleep(numero);
                            break;
                        }
                    }
                } catch (Exception ex) {

                }
                j++;
                String id_factura = "";
                String[] datos = linea.split(";");
                String cups = datos[0];
                String fecha_parseada = datos[1].replaceAll("/", "-");
                String[] fecha_hora = fecha_parseada.split(" ");//0 fecha 1 hora
                String[] split_hora = fecha_hora[1].split(":");//0 hora 1 minutos
                String[] fecha_split = fecha_hora[0].split("-");
                int hora_int = Integer.parseInt(split_hora[0]);
                int dia = Integer.parseInt(fecha_split[2]);
                int anio = Integer.parseInt(fecha_split[0]);
                int mes = Integer.parseInt(fecha_split[1]);
                if (fichero.getName().contains("F5D"))
                    id_factura = datos[11].trim();
                JsonObject hora_obj = new JsonObject();
                cch_actual = new JsonObject()

                        .add("cups", cups)
                        .add("dia", dia)
                        .add("mes", mes)
                        .add("anio", anio)
                        .add("timestamp", fichero.lastModified() / 1000)
                        .add("archivos", new JsonArray().add(fichero.getName()));
                if (id_factura != "")
                    cch_actual.add("id_factura", id_factura);
                if (fichero.getName().toLowerCase().contains("f5d")) {
                    hora_obj = new JsonObject()
                            .add("energiaEntrante", Integer.parseInt(datos[3]))
                            .add("energiaSaliente", Integer.parseInt(datos[4]))
                            .add("energiaReactiva_1", Integer.parseInt(datos[5]))
                            .add("energiaReactiva_2", Integer.parseInt(datos[6]))
                            .add("energiaReactiva_3", Integer.parseInt(datos[7]))
                            .add("energiaReactiva_4", Integer.parseInt(datos[8]))
                            .add("obtencion", Integer.parseInt(datos[9]))
                            .add("firmeza", Integer.parseInt(datos[10]));
                } else {
                    try {
                        hora_obj = new JsonObject()
                                .add("energiaEntrante", Integer.parseInt(datos[3]));
                        if (!datos[4].isEmpty())
                            hora_obj.add("energiaSaliente", Integer.parseInt(datos[4]));
                        else
                            hora_obj.add("energiaSaliente", 0);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
                int tipo_curva = 0;
                if (fichero.getName().toLowerCase().contains("p5d"))
                    tipo_curva = 1;
                if (!this.Existe(conexion, cups, dia, mes, anio, tipo_curva)) {

                    cch_actual.add("hora" + hora_int, hora_obj);
                    collection.insertOne(Document.parse(cch_actual.toString()));
                } else {
                    collection.updateOne(Filters.and(
                            Filters.eq("dia", dia),
                            Filters.eq("mes", mes),
                            Filters.eq("anio", anio),
                            Filters.regex("cups", cups.substring(0, 20))
                            ),
                            Document.parse(new JsonObject().add("$set", new JsonObject().add("hora" + hora_int, hora_obj)).toString())
                    );
                    if (collection.countDocuments(Filters.and(Filters.eq("dia", dia),
                            Filters.eq("mes", mes),
                            Filters.eq("anio", anio),
                            Filters.regex("cups", cups.substring(0, 20)))) == 0) {
                        collection.updateOne(Filters.and(
                                Filters.eq("dia", dia),
                                Filters.eq("mes", mes),
                                Filters.eq("anio", anio),
                                Filters.regex("cups", cups.substring(0, 20))
                                ),
                                Document.parse(new JsonObject().add("$push", new JsonObject().add("archivos", fichero.getName())).toString())
                        );
                    }
                }
            }
            this.EliminarFichero(fichero);

        }
    }


    //tipo_curva 0, F5D, 1- P5D
    public boolean Existe (Mongo conexion, String cups, int dia, int mes, int anio, int tipo_curva){
        if ( tipo_curva == 0) {
            long countDocuments_Facturables = conexion.getCCHFacturables().countDocuments(
                    Filters.and(
                            Filters.eq("dia", dia),
                            Filters.eq("mes", mes),
                            Filters.eq("anio", anio),
                            Filters.regex("cups", cups.substring(0, 20))
                    )
            );
            if ( countDocuments_Facturables == 0){
                return false;
            }else{
                return true;
            }
        }else {
            long countDocuments_Validadas = conexion.getCCHValidadas().countDocuments(
                    Filters.and(
                            Filters.eq("dia", dia),
                            Filters.eq("mes", mes),
                            Filters.eq("anio", anio),
                            Filters.regex("cups", cups.substring(0, 20))
                    )
            );
            if ( countDocuments_Validadas == 0){
                return false;
            }else{
                return true;
            }
        }
    }

    private void EliminarFichero (File absolute_path ){
        absolute_path.delete();
    }
}
